package hw2;

import org.junit.jupiter.api.*;

import java.util.Random;

public class CalculatorTest {

    private Calculator calculator;

    @BeforeEach
    public void createCalculatorInstance() {
        calculator = new Calculator();
    }

    @Test
    public void testAdition() {
        Random random = new Random();
        int a = random.nextInt(100);
        int b = random.nextInt(100);
        Assertions.assertEquals(a + b, calculator.add(a , b));
    }

    @Test
    public void testSubtraction() {
        Random random = new Random();
        int a = random.nextInt(100);
        int b = random.nextInt(100);
        Assertions.assertEquals(a - b, calculator.subtract(a , b));
    }

    @Test
    public void testMultiplication() {
        Random random = new Random();
        int a = random.nextInt(100);
        int b = random.nextInt(100);
        Assertions.assertEquals(a * b, calculator.multiply(a , b));
    }

    @Test
    public void testDivision() {
        Random random = new Random();
        int a = random.nextInt(100);
        int b = random.nextInt(100) + 1;
        Assertions.assertEquals(a / b, calculator.divide(a , b));
    }

    @Test
    public void testDivisionByZero() {
        Random random = new Random();
        int a = random.nextInt(100);
        int b = 0;
        Exception actualException = Assertions.assertThrows(Exception.class, () -> {
            calculator.divide(a, b);
        });
        Assertions.assertEquals("Cannot divide by zero!", actualException.getMessage());
    }
}
